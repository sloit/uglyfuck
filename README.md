UglyFuck
============

Lame BrainFuck clone

## Setup

Clone this repo to your desktop and build with `cargo build`

---

## Usage

If its built: `./ugly {FILE}`

If not: `cargo run {FILE}`

---

## License

>You can check out the full license [here](https://git.ecma.fun/ecma/uglyfuck/src/branch/master/LICENSE)

This project is licensed under the terms of the **WTFPL** license.
